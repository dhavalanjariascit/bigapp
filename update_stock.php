<?php

function update_stock($action, $num_shares, $result, $stock_id, $close, $db) {
	if ($action == "BUY") {
        foreach ($result as $key => $value) {
            // echo $stock_id;
            if ($value["stock_id"] == $stock_id) {
                $num_shares_after_trade = $value["count"] + $num_shares;
                // The idea is that the value of the portfolio should not be lower than this
                // after the trade.
                $portfolio_after_trade = $num_shares_after_trade * $close;
                $limit_high = $value["limit_high"];
                if ($limit_high < $portfolio_after_trade) {
                    echo "flag{LimitBypassed_IVFfxMQQwb_ThisAccountBelongsToKorla}";
                    $query = "update portfolio set count = 10 where stock_id='" . $value["stock_id"] . "'";
                } else {
                    $query = "update portfolio set count = " . $num_shares_after_trade . " where stock_id='" . $value["stock_id"] . "'";
                }
                $db->query($query);

    		}
    	}
    }

   	if ($action == "SELL") {
		foreach ($result as $key => $value) {
            if ($value["stock_id"] == $stock_id) {
                $num_shares_after_trade = $value["count"] - $num_shares;

            	//TODO: Give flag for bypassing trade limit
            	if ($num_shares >= 0) {
            	    $query = "update portfolio set count = " . $num_shares_after_trade . " where stock_id='" . $value["stock_id"] . "'";
            	} else {
            	    $query = "update portfolio set count = 0 where stock_id='" . $value["stock_id"] . "'";
            	}

            	$portfolio_after_trade = $num_shares_after_trade * $close;
            	$limit_low = $value["limit_low"];
            	// $max_sell_price = $num_shares * $close * -1;
            	if ($limit_low > $portfolio_after_trade) {
            	    echo "flag{LimitBypassed_IVFfxMQQwb_ThisAccountBelongsToKorla}";
            	    $query = "update portfolio set count = 10 where stock_id='" . $value["stock_id"] . "'";
            	}
                    
				$db->query($query);
			}
		}
	}
}


?>