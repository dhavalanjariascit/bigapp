<html lang="en">
<?php session_start();

$username = "";
if (isset($_SESSION['login'])) {
    $username = $_SESSION['login'];
} 
?>
    <meta charset="utf-8">

    <title>Stocks</title>
    <meta name="description" content="INSERT SITE DESCRIPTION HERE">
    <meta name="author" content="INSERT CONTENT HERE">

    <link rel="stylesheet" href="">


                <!--[if lt IE 9]>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
                    <![endif]-->

                    <!-- Bootstrap stuff -->
                    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

                    <!-- Latest compiled JavaScript, JQuery and Popper.js -->

    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

                    <!-- So it works on mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1">


                    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <!-- AlphaVantage JS API -->
    <script src="https://cdn.jsdelivr.net/npm/alphavantage@2.0.0/dist/bundle.min.js"></script>
    <style>
    	.trades-table {
    		position: absolute;
    		max-width: 70%;
    	}
    </style>



</head>
<body>
    <?php
        include("nav.php");
    ?>

</div>
<div class="container">
  <script>
    $(function() {
      var apiKey = "NWZ88KTVYIC94ALT";
      symbols = document.getElementsByClassName('symbol');
      for (let curr_td of symbols) {
        var symbol = curr_td.innerHTML;
        url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + symbol + "&apikey=" + apiKey;
        $.ajax({
          url: "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + symbol + "&apikey=" + apiKey
        }).done(function( data ) {
        curr_symbol = data["Meta Data"]["2. Symbol"];
        last_day = Object.keys(data["Time Series (Daily)"])[0];

        open = data["Time Series (Daily)"][last_day]["1. open"];
        high = data["Time Series (Daily)"][last_day]["2. high"];
        low = data["Time Series (Daily)"][last_day]["3. low"];
        close = data["Time Series (Daily)"][last_day]["4. close"];
        volume = data["Time Series (Daily)"][last_day]["5. volume"];
        var open_td = document.getElementById(curr_symbol + "-open");
        var high_td = document.getElementById(curr_symbol + "-high");
        var low_td = document.getElementById(curr_symbol + "-low");
        var close_td = document.getElementById(curr_symbol + "-close");
        var volume_td = document.getElementById(curr_symbol + "-volume");
        open_td.innerHTML = open;
        high_td.innerHTML = high;
        low_td.innerHTML = low;
        close_td.innerHTML = close;
        volume_td.innerHTML = volume;
        });
      }
      
    });
  </script>
<!-- TradingView Widget END -->
    <?php
      require('db.php');
      $db = new DB();
      if (isset($_SESSION['login'])) {
        $username = $_SESSION['login'];
        $db->connect();
      } else {
        header("Location: index.php");
      }
        
      if ($_SERVER['REQUEST_METHOD'] === "POST") {
        $stock_id = $_POST['stock'];
        $description = $_POST['description'];
        $query = "insert into stocks(user, stock_id, description) values('" . $username . "', '" . $stock_id . "', '" . $description . "');";
        // echo $query;
        $result = $db->query($query);
      } 
    ?>
	
    <table class="table table-striped mt-5">
      <tr>
        <th>SYMBOL</th>
        <th>DESCRIPTION</th>
        <th>OPEN</th>
        <th>HIGH</th>
        <th>LOW</th>
        <th>CLOSE</th>
        <th>VOLUME</th>
      </tr>
      <?php
        $query = "SELECT * FROM stocks WHERE user='" . $username . "';";

        $result = $db->select($query);
        if ($result != false) {
          foreach($result as $key => $value) {
            echo "<tr>";
            echo "<td class='symbol' id='". $value["stock_id"] . "'>". $value["stock_id"] . "</td>";
            echo "<td class='symbol' id='". $value["description"] . "'>". $value["description"] . "</td>";
            echo "<td id='" . $value["stock_id"] . "-open'></td>";
            echo "<td id='" . $value["stock_id"] . "-high'></td>";
            echo "<td id='" . $value["stock_id"] . "-low'></td>";
            echo "<td id='" . $value["stock_id"] . "-close'></td>";
            echo "<td id='" . $value["stock_id"] . "-volume'></td>";
            echo "</tr>";
          }
        }
      ?>
    </table>

</div>
<div class="container">
  <!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <div class="tradingview-widget-copyright"><a href="https://in.tradingview.com" rel="noopener" target="_blank"><span class="blue-text">Market Data</span></a> by TradingView</div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
  {
  "colorTheme": "dark",
  "dateRange": "1d",
  "showChart": true,
  "locale": "in",
  "width": "100%",
  "height": "100%",
  "largeChartUrl": "",
  "isTransparent": true,
  "plotLineColorGrowing": "rgba(25, 118, 210, 1)",
  "plotLineColorFalling": "rgba(25, 118, 210, 1)",
  "gridLineColor": "rgba(42, 46, 57, 1)",
  "scaleFontColor": "rgba(120, 123, 134, 1)",
  "belowLineFillColorGrowing": "rgba(33, 150, 243, 0.12)",
  "belowLineFillColorFalling": "rgba(33, 150, 243, 0.12)",
  "symbolActiveColor": "rgba(182, 182, 182, 0.12)",
  "tabs": [
    {
      "title": "Indices",
      "symbols": [
        {
          "s": "OANDA:SPX500USD",
          "d": "S&P 500"
        },
        {
          "s": "OANDA:NAS100USD",
          "d": "Nasdaq 100"
        },
        {
          "s": "FOREXCOM:DJI",
          "d": "Dow 30"
        },
        {
          "s": "OANDA:UK100GBP",
          "d": "FTSE 100"
        },
        {
          "s": "NSE:NIFTY"
        },
        {
          "s": "BSE:SENSEX"
        }
      ],
      "originalTitle": "Indices"
    },
    {
      "title": "Commodities",
      "symbols": [
        {
          "s": "CME_MINI:ES1!",
          "d": "E-Mini S&P"
        },
        {
          "s": "CME:6E1!",
          "d": "Euro"
        },
        {
          "s": "COMEX:GC1!",
          "d": "Gold"
        },
        {
          "s": "NYMEX:CL1!",
          "d": "Crude Oil"
        },
        {
          "s": "NYMEX:NG1!",
          "d": "Natural Gas"
        },
        {
          "s": "CBOT:ZC1!",
          "d": "Corn"
        }
      ],
      "originalTitle": "Commodities"
    },
    {
      "title": "Bonds",
      "symbols": [
        {
          "s": "CME:GE1!",
          "d": "Eurodollar"
        },
        {
          "s": "CBOT:ZB1!",
          "d": "T-Bond"
        },
        {
          "s": "CBOT:UB1!",
          "d": "Ultra T-Bond"
        },
        {
          "s": "EUREX:FGBL1!",
          "d": "Euro Bund"
        },
        {
          "s": "EUREX:FBTP1!",
          "d": "Euro BTP"
        },
        {
          "s": "EUREX:FGBM1!",
          "d": "Euro BOBL"
        }
      ],
      "originalTitle": "Bonds"
    },
    {
      "title": "Forex",
      "symbols": [
        {
          "s": "FX:EURUSD"
        },
        {
          "s": "FX:GBPUSD"
        },
        {
          "s": "FX:USDJPY"
        },
        {
          "s": "FX:USDCHF"
        },
        {
          "s": "FX:AUDUSD"
        },
        {
          "s": "FX:USDCAD"
        }
      ],
      "originalTitle": "Forex"
    }
  ]
}
  </script>
  </div>
</body>
</html>