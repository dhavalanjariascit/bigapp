<?php
/* 
 * This file defines values that will be used by all files. All they have to do is include this file.
 * 
 * The file contains database login credentials, global variables that will be used in includes by all the files, etc.
 * 
 * It should be located in the root directory of the project, i.e. the top-level directory of the project, not the server.
 */
   
define("ROOT_DIR", __DIR__);
// Database definitions
define("DB_USER", "root");
define("DB_PASSWORD", "password123");
define("DB_DATABASE", "bigappdb");
define("DB_HOST", "127.0.0.1");
define("DB_PORT", "3306");
