FROM mattrayner/lamp:latest-1804

ENV MYSQL_DATABASE=bigappdb

ENV MYSQL_ROOT_PASSWORD=password123

ADD bigappdb.sql /docker-entrypoint-initdb.d/bigappdb.sql

ENTRYPOINT ["./run.sh"]


