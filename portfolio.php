<html lang="en">
<?php
require('db.php');

require('update_stock.php');
session_start();
$username = "";
if (isset($_SESSION['login'])) {
    $username = $_SESSION['login'];
} else {
    echo "session not set";
}

?>
    <meta charset="utf-8">

    <title>Trades</title>
    <meta name="description" content="INSERT SITE DESCRIPTION HERE">
    <meta name="author" content="INSERT CONTENT HERE">

    <link rel="stylesheet" href="">


                <!--[if lt IE 9]>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
                    <![endif]-->

                    <!-- Bootstrap stuff -->
                    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

                    <!-- Latest compiled JavaScript, JQuery and Popper.js -->

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

                    <!-- So it works on mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1">


                    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <style>
    	.trades-table {

    	}
    </style>

    <script>
        // Make sure that trades stay within buy limit and sell limit.
        $(function() {
            // First get prices
          var apiKey = "NWZ88KTVYIC94ALT";
          symbols = document.getElementsByClassName('symbol');
          for (let curr_td of symbols) {
            var symbol = curr_td.innerHTML;
            url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + symbol + "&apikey=" + apiKey;
            $.ajax({
              url: "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + symbol + "&apikey=" + apiKey
            }).done(function( data ) {
            curr_symbol = data["Meta Data"]["2. Symbol"];
            
            last_day = Object.keys(data["Time Series (Daily)"])[0];

            close = data["Time Series (Daily)"][last_day]["4. close"];
            var close_td = document.getElementById(curr_symbol + "-close");

            close_td.innerHTML = close;

            var shares_owned = document.getElementById(curr_symbol + '-count').innerHTML;

            // Set portfolio value. Done here so that it doesn't crash the backend if this fails.
            portfolio_value = document.getElementById(curr_symbol + '-portfolio_value');
            portfolio_value.innerHTML = close * shares_owned;


            // Once we get the price, set this * buy limit as the max number of shares you can buy
            var num_shares_buy = document.getElementById(curr_symbol + '-buy-num_shares');
            var num_shares_sell = document.getElementById(curr_symbol + '-sell-num_shares');

            var portfolio_min = document.getElementById(curr_symbol + '-limit_low').innerHTML;
            var portfolio_max = document.getElementById(curr_symbol + '-limit_high').innerHTML;

            var close = document.getElementById(curr_symbol + '-close').innerHTML;

            num_shares_buy.max = (portfolio_max / close) - shares_owned;
            num_shares_sell.max = shares_owned - portfolio_min / close;

            if (num_shares_buy.max < 0 ) {
                num_shares_buy.max = 0;
            }


            if (num_shares_sell.max < 0 ) {
                num_shares_sell.max = 0;
            }


            // Add the prices to the forms.
            var buy_input = document.getElementById(curr_symbol + '-form-buy-price');
            var sell_input = document.getElementById(curr_symbol + '-form-sell-price');

            buy_input.value = close;
            sell_input.value = close;


            });
          }


      
    });
    </script>

</head>
<body>

    <?php
        include("nav.php");
    ?>
	<div class="container mt-4">

    <div class="alert alert-success">flag{YouFoundKorla_SqlInjectionSuccessful}</div>
	<table class="table trades-table table-bordered mt-5 table-responsive">

		<tr>
			<th>Symbol</th>
			<th>Shares owned</th>
			<th>Curr. Price Per Share</th>
            <th>Curr. value of shares owned</th>
            <th>Portfolio Min Value (in USD)</th>
			<th>Portfolio Max Value (in USD)</th>
			<th>Buy</th>
            <th>Sell</th>
		</tr>
		<?php
            $db = new DB();
            $db->connect();

            $query = "SELECT * from portfolio where stock_id in (select stock_id from stocks where user='" . $username . "');";
            $result = $db->select($query);
            
            if ($_SERVER['REQUEST_METHOD'] === "POST") {
                $num_shares = $_POST['num_shares'];


                $action = $_POST['position'];
                $stock_id = $_POST['stock_id'];
                $close = $_POST['close_price'];
                $max_buy_price = 0;
                $max_sell_price = 0;

                if ($num_shares < 0) {
                    echo "<p class='text-danger'>Error: Number of shares cannot be negative</p><br/>";

                } else {

                    update_stock($action, $num_shares, $result, $stock_id, $close, $db);
                }
            }
            
            if($result != false) {
                $query = "SELECT * from portfolio where stock_id in (select stock_id from stocks where user='" . $username . "');";
                $result = $db->select($query);
                foreach ($result as $key => $value) {
                    echo "<tr>";
                    $stock_id = $value["stock_id"];
                    echo "<td id='$stock_id' class='symbol'>$stock_id</td>";   
                    echo "<td id='$stock_id-count'>$value[count]</td>";   
                    echo "<td id='$stock_id-close'>0</td>";   
                    echo "<td id='$stock_id-portfolio_value'></td>";
                    echo "<td id='$stock_id-limit_low'>$value[limit_low]</td>";   
                    echo "<td id='$stock_id-limit_high'>$value[limit_high]</td>";   
                        // if ($col == "stock_id") {
                        //     echo "<td id='$value[stock_id]' class='symbol'>$value[stock_id]</td>";   
                        // }
                        // else if ($col == "price") {
                        //     echo "<td id='$value[stock_id]-close'>$value[price]</td>";   
                        // } else if ($col == "limit_low") {
                        //     echo "<td id='$value[stock_id]-limit_low'>$value[limit_low]</td>";   
                        // } else if ($col == "limit_high") {
                        //     echo "<td id='$value[stock_id]-limit_high'>$value[limit_high]</td>";   
                        // } else if ($col == "count") {
                        //     echo "<td id='$value[stock_id]-count'>$value[count]</td>";   
                        // }

                    echo "<td><form id='$value[stock_id]-buy-form' action='portfolio.php' method='POST'>
                                <input id='$value[stock_id]-buy-num_shares' name='num_shares' type='number'/>
                                <input type='hidden' name='stock_id' value='" . $value["stock_id"] . "'/>
                                <input type='hidden' id='$value[stock_id]-form-buy-price' min='0' name='close_price' value='' />
                                <input type='submit' name='position' value='BUY' class='btn btn-sm btn-outline-danger my-2'>
                                </form>
                        </td>";
                    echo "<td><form id='$value[stock_id]-sell-form' action='portfolio.php' method='POST'>
                                <input id='$value[stock_id]-sell-num_shares' name='num_shares' type='number'/>
                                <input type='hidden' name='stock_id' value='" . $value["stock_id"] . "'/>
                                <input type='hidden' id='$value[stock_id]-form-sell-price' min='0' name='close_price' value='' />
                                <input type='submit' name='position' value='SELL' class='btn btn-sm btn-outline-danger my-2'>
                                </form>
                        </td>";

                    echo "</tr>";
                }
            }
		?>
	</table>
</div>
</body>
</html>
