<html lang="en">
<head>

    <?php
	require('db.php');

	$invalid_credentials = False;

        if (isset($_SESSION['login'])) {
            session_destroy();
        } 

        if ($_SERVER['REQUEST_METHOD'] === "POST") {
            $username = $_POST['username'];
            $password = $_POST['password'];

            $query = "select username, password from users where username = '" . $username . "' and password = '" . $password . "';";
            $db = new DB();
            $db->connect();
	    $result = $db->select($query);
	    echo $result;
	    if (count($result) > 0) {
                session_start();
                $_SESSION['login'] = $result[0]["username"];
               
                header('Location: portfolio.php');
            } else {
                $invalid_credentials = True;
            }
        }
    ?>

    <meta charset="utf-8">

    <title>BigApp</title>
    <meta name="description" content="INSERT SITE DESCRIPTION HERE">
    <meta name="author" content="INSERT CONTENT HERE">

    <link rel="stylesheet" href="">


                <!--[if lt IE 9]>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
                    <![endif]-->

                    <!-- Bootstrap stuff -->
                    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

                    <!-- Latest compiled JavaScript, JQuery and Popper.js -->

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

                    <!-- So it works on mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1">


                    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <style>
        .investing-img {
            height: auto;
            width: 80%;
        }

    </style>

</head>
<body>
    <div class="jumbotron bg-danger">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="text-white">EVOLV<i>e</i> Corporation</h1>
                    <h3 class="text-white">value uber alles</h3>
                </div>
                <div class="col-md-6">
                    <img src="invest.png" class="investing-img" />
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-12">
		<h1 class="text-center text-danger">Log In</h1>
		<?php
			if ($invalid_credentials == True) {
				echo "<div class='alert alert-danger'>Invalid credentials</div>";
			}

		?>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-12">
                <form action="index.php" class="text-center rounded border border-danger p-4" method="POST">
                  <div class="form-group ">
                    <input type="text" class="form-control" placeholder="Enter username" id="username" name="username">
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control form-control-danger" placeholder="Enter password" id="pwd" name="password">
                  </div>
                  <button type="submit" name="submit" class="btn btn-sm btn-outline-danger">Submit</button>
                </form>
            </div>
    </div>

    
    
</body>

</html>
