#!/bin/bash

docker stop $(sudo docker ps -aq --filter name=bigapp_lamp)
docker rm $(sudo docker ps -aq --filter name=bigapp_lamp)
docker system prune -af
