#!/bin/bash

cd /home/dhaval/bigapp/

# Cleanup
docker stop $(sudo docker ps -aq --filter name=bigapp_lamp) 
docker rm $(sudo docker ps -aq --filter name=bigapp_lamp) 
docker system prune -af

docker run -d -p "80:80" --name bigapp_lamp  -v ${PWD}:/app mattrayner/lamp:latest


if [[ $(docker container inspect -f '{{ .State.Status }}' bigapp_lamp) == "running" ]]
then

	sleep 30 # Wait for docker to spin up
	echo "Checking mysql..."
	res=1
	while [[ $res != 0 ]]
	do
		echo "Creating bigappdb... Trial #$count"
		docker exec -it bigapp_lamp mysql -u root -h 127.0.0.1 -P 3306 -e "create database bigappdb; use bigappdb; source /app/bigappdb.sql; alter user 'root'@'localhost' identified by 'password123'; use bigappdb; show tables;"
		res=$(echo $?)
		count=$((count + 1))
		if [[ $count -gt 5 ]]
		then
			echo "MySQL connection unsuccessful. Something is wrong with the containers"
		exit 1
		fi
		sleep 5
	done
	

#	echo "Copy SQL backup script..."
#	docker cp ${PWD}/bigappdb.sql bigapp_lamp:/app/bigappdb.sql
#
#	echo "Running backup script..."
#	docker exec -dt bigapp_lamp mysql -u root -hlocalhost -P3306 bigappdb -e "source /app/bigappdb.sql"
#
#	echo "Setting root password for mysql..."
#	docker exec -dt bigapp_lamp mysql -u root -hlocalhost -P3306 -e "alter user 'root'@'localhost' identified by 'password123';"

#	echo "Checking tables..."
#	docker exec -dt bigapp_lamp mysql -u root -ppassword123 -hlocalhost -P3306 bigappdb -e "show tables;"

else
	exit 1
fi


