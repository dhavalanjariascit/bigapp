#!/bin/bash

cd /home/dhaval/bigapp/

docker exec -it bigapp_lamp mysql -u root -ppassword123 -h 127.0.0.1 -P 3306 -e "drop database bigappdb;"

docker exec -it bigapp_lamp mysql -u root -ppassword123 -h 127.0.0.1 -P 3306 -e "create database bigappdb; use bigappdb; source /app/bigappdb.sql; alter user 'root'@'localhost' identified by 'password123'; use bigappdb; show tables;"
